# Burger Builder v1.0

###### Overview: Create a delicious burger, place and check your orders.

**Demo: [burger-builder.app](https://burgerbuilder-test.web.app/)**   

## Runbook
#### Set Dev Environment
- Install 12 version  
    __[node.js](http://nodejs.org/)__
- Install yarn  
- Install project dependencies  
```
yarn install
```

#### Add API key env variable to .env.local
```
REACT_APP_AUTH_API_KEY = 12345
```

#### Run locally
```
yarn start
```

#### Build for production
```
yarn build
```

#### Deploy via Firebase
```
yarn global add firebase-tools
firebase login
firebase init
firebase deploy
```
    
## What's inside 
1. CSS modules
2. Webpack
3. ES7
4. Axios
5. React
6. Redux
7. Firebase

## TODO
#### WIP
- Refactor: helpers
- Refactor: forms
- Refactor: components
- Refactor: actions
- Refactor: reducers
- Split controllers & containers
- Split actions & action creators
- Actions: Get rid of generic payload object
- Rename files in camelCase
- Unit testing
- Files prettifying
- Code linting

#### Optional
- Modern lazy loading
- Validation messages
- Success messages
- Alert component with dismiss || auto-dismiss
- Component requestError handling
- Error pages

#### Upgrade 1
- React hooks
- New design
- Fonts
- SCSS
- Packages (react-icons, react-moment, lodash, react-bootstrap/semantic UI)
- Animation

#### Upgrade 2
- Create __create-env.sh__ shell script, add instructions
- Create express REST API server
- Replace Firebase with MongoDB
- Create __create-db.js__ MongoDB shell script
- Secure DB

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).