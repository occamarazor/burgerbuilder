import PropTypes from 'prop-types';

const IAlertProps = {
  children: PropTypes.any.isRequired
};

export default IAlertProps;
