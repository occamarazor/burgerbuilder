import React       from 'react';
import styles      from './alert.module.css';
import IAlertProps from './types';

const Alert = ({ children }) => <h3 className={styles.DataEmpty}>{children}</h3>;

Alert.propTypes = IAlertProps;

export default Alert;
