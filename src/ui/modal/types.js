import PropTypes from 'prop-types';

const IModalProps = {
  isVisible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  children: PropTypes.element
};

export default IModalProps;
