import React, { Component } from 'react';
import { Backdrop }         from '../';
import styles               from './modal.module.css';
import IModalProps          from './types';

class Modal extends Component {
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return this.props.isVisible !== nextProps.isVisible;
  }
  
  render() {
    const { isVisible, onClose, children } = this.props;
    const modalStyles = `${styles.Modal} ${isVisible ? styles.Visible : styles.Hidden}`;
    
    return (
      <>
        <div className={modalStyles}>
          {children}
        </div>
        <Backdrop isVisible={isVisible} click={onClose}/>
      </>
    );
  };
}

Modal.propTypes = IModalProps;

export default Modal;
