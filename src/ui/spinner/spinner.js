import React  from 'react';
import styles from './spinner.module.css';

const Spinner = () => (
  <div className={styles.Wrapper}>
    <div className={styles.Spinner}/>
  </div>
);

export default Spinner;
