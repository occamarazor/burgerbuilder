import PropTypes from 'prop-types';

const IFormFieldProps = {
  name: PropTypes.string.isRequired,
  element: PropTypes.string.isRequired,
  config: PropTypes.exact({
    type: PropTypes.string,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    defaultValue: PropTypes.string,
    options: PropTypes.arrayOf(
      PropTypes.exact({
        value: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired
      }).isRequired
    ),
    validation: PropTypes.exact({
      required: PropTypes.bool.isRequired,
      minLength: PropTypes.number,
      maxLength: PropTypes.number,
      isEmail: PropTypes.string
    })
  }).isRequired,
  value: PropTypes.string.isRequired,
  isValid: PropTypes.bool.isRequired,
  isTouched: PropTypes.bool.isRequired,
  onFieldChange: PropTypes.func.isRequired
};

export default IFormFieldProps;
