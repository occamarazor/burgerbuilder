import React           from 'react';
import styles          from './form-field.module.css';
import IFormFieldProps from './types';

const FormField = ({ name, element, config, value, isValid, isTouched, onFieldChange }) => {
  const { type, label, placeholder, options } = config;
  const invalidStyle = !isValid && isTouched ? styles.Invalid : '';
  let fieldStyles = `${styles.Field} ${invalidStyle}`;
  let formField;
  
  switch(element) {
    case('select'):
      fieldStyles = `${fieldStyles} ${styles.Select}`;
      formField = (
        <select name={name}
                id={name}
                value={value}
                onChange={onFieldChange}
                className={fieldStyles}>
          {options.map((o) => (
            <option key={o.value} value={o.value}>{o.label}</option>
          ))}
        </select>
      );
      break;
    case('textarea'):
      fieldStyles = `${fieldStyles} ${styles.Textarea}`;
      formField = (
        <textarea name={name}
                  id={name}
                  value={value}
                  placeholder={placeholder}
                  rows="3"
                  onChange={onFieldChange}
                  className={fieldStyles}/>
      );
      break;
    default:
      formField = (
        <input type={type}
               name={name}
               id={name}
               value={value}
               placeholder={placeholder}
               onChange={onFieldChange}
               className={fieldStyles}/>
      );
  }
  
  return (
    <div className={styles.Wrapper}>
      <label className={styles.Label} htmlFor={name}>
        {label}
      </label>
      {formField}
    </div>
  );
};

FormField.propTypes = IFormFieldProps;

export default FormField;
