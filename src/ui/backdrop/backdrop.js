import React, { Component } from 'react';
import styles               from './backdrop.module.css';
import IBackdropProps       from './types';

class Backdrop extends Component {
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return this.props.isVisible !== nextProps.isVisible;
  }
  
  render() {
    const { isVisible, click } = this.props;
    return isVisible && <div className={styles.Backdrop} onClick={click}/>;
  }
}

Backdrop.propTypes = IBackdropProps;

export default Backdrop;
