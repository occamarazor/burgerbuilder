import PropTypes from 'prop-types';

const IBackdropProps = {
  isVisible: PropTypes.bool.isRequired,
  click: PropTypes.func.isRequired
};

export default IBackdropProps;
