import PropTypes from 'prop-types';

const IButtonProps = {
  classNames: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  disable: PropTypes.bool,
  click: PropTypes.func.isRequired,
  children: PropTypes.string
};

export default IButtonProps;
