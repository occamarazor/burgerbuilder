import React, { PureComponent } from 'react';
import styles                   from './button.module.css';
import IButtonProps             from './types';

class Button extends PureComponent {
  render() {
    const { classNames, disable, click, children } = this.props;
    const buttonStyles = classNames.reduce((str, name) => `${str} ${styles[name]}`, styles.Button);
    
    return (
      <button className={buttonStyles} disabled={disable} onClick={click}>
        {children}
      </button>
    );
  }
}

Button.propTypes = IButtonProps;

export default Button;
