const ROOT = '/';
const LOGIN = '/login';
const LOGOUT = '/logout';
const CHECKOUT = '/checkout';
const CHECKOUT_FORM = '/checkout/form';
const ORDERS = '/orders';

export {
  LOGIN,
  LOGOUT,
  ROOT,
  CHECKOUT,
  CHECKOUT_FORM,
  ORDERS
};