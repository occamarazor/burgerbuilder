import React                  from 'react';
import { routerPaths }        from './';
import { loadComponentAsync } from '../hoc';
import { Alert }              from '../ui';

const asyncBuilder = loadComponentAsync(() => import('../features/product-builder'));
const asyncLogin = loadComponentAsync(() => import('../features/product-login'));
const asyncLogout = loadComponentAsync(() => import('../features/product-logout'));
const asyncCheckout = loadComponentAsync(() => import('../features/product-checkout'));
const asyncCheckoutForm = loadComponentAsync(() => import('../features/product-checkout/components'));
const asyncOrders = loadComponentAsync(() => import('../features/product-orders'));

const publicRoutes = [
  {
    path: routerPaths.ROOT,
    exact: true,
    component: asyncBuilder
  },
  {
    path: routerPaths.LOGIN,
    exact: true,
    component: asyncLogin
  }
];

const restrictedRoutes = [
  {
    path: routerPaths.LOGOUT,
    exact: true,
    component: asyncLogout
  },
  {
    path: routerPaths.CHECKOUT,
    component: asyncCheckout,
    routes: [
      {
        path: routerPaths.CHECKOUT_FORM,
        exact: true,
        component: asyncCheckoutForm
      }
    ]
  },
  {
    path: routerPaths.ORDERS,
    exact: true,
    component: asyncOrders
  }
];

const errorRoutes = [
  {
    path: routerPaths.ROOT,
    // TODO: async 404 page
    component: () => <Alert>Error: 404. Page not found.</Alert>
  }
];

const unauthorizedRoutes = [
  ...publicRoutes,
  ...errorRoutes
];

const authorizedRoutes = [
  ...publicRoutes,
  ...restrictedRoutes,
  ...errorRoutes
];

export { unauthorizedRoutes, authorizedRoutes };
