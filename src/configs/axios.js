import axios from 'axios';

const app = axios.create({
  baseURL: 'https://burgerbuilder-test.firebaseio.com/'
});

const auth = axios.create({
  baseURL: 'https://identitytoolkit.googleapis.com/v1/'
});

export { app, auth };
