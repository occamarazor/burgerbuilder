// TODO: get rid off .config prop
// TODO: get rid off .element prop
// TODO: extend validation object with message
// validation: { required: { message: 'Field should not be empty', value: true } }
import React         from 'react';
import { FormField } from '../ui';

const emailValidationPattern = '^.+@.+\\..+$';

const isFormValidPropName = (formName) => `is${formName}Valid`;

const formConfigToFormState = (formName, formConfig) => ({
  [formName]: Object
    .entries(formConfig)
    .reduce((formState, [ fieldName, fieldConfig ]) => ({
      ...formState,
      [fieldName]: {
        value: fieldConfig.config.defaultValue || '',
        isValid: !!fieldConfig.config.defaultValue,
        isTouched: false
      }
    }), {}),
  [isFormValidPropName(formName)]: false
});

const formConfigToFormView = (formName, formConfig, formState, fieldChangeHandler) =>
  Object
    .entries(formConfig)
    .map(([ fieldName, fieldConfig ]) => (
      <FormField key={fieldName}
                 name={fieldName}
                 element={fieldConfig.element}
                 config={fieldConfig.config}
                 value={formState[formName][fieldName].value}
                 isValid={formState[formName][fieldName].isValid}
                 isTouched={formState[formName][fieldName].isTouched}
                 onFieldChange={(e) => fieldChangeHandler(fieldName, e.target.value)}/>
    ));



const updateFormValidationState = (formName, formState) => ({
  [isFormValidPropName(formName)]: Object
    .values(formState[formName])
    .reduce((isPrevFieldValid, fieldState) =>
      isPrevFieldValid && fieldState.isValid,
      true
    )
});

const updateFieldValidationState = (validationPatterns, fieldValue) => {
  let isValid = true;
  
  if(validationPatterns.required) {
    isValid = fieldValue.trim() !== '' && isValid;
  }
  
  if(validationPatterns.minLength) {
    isValid = fieldValue.length >= validationPatterns.minLength && isValid;
  }
  
  if(validationPatterns.maxLength) {
    isValid = fieldValue.length <= validationPatterns.maxLength && isValid;
  }
  
  if(validationPatterns.isEmail) {
    isValid = new RegExp(validationPatterns.isEmail).test(fieldValue) && isValid;
  }
  
  return isValid;
};

const updateFormFieldState = (formName, formConfig, formState, fieldName, fieldValue) => ({
  [formName]: {
    ...formState[formName],
    [fieldName]: {
      value: fieldValue,
      isValid: formConfig[fieldName].config.validation
        ? updateFieldValidationState(formConfig[fieldName].config.validation, fieldValue)
        : formState[formName][fieldName].isValid,
      isTouched: true
    }
  }
});

const formStateToSubmitData = (formName, formState) =>
  Object
    .entries(formState[formName])
    .reduce((formData, [ fieldName, fieldState ]) => ({
      ...formData,
      [fieldName]: fieldState.value
    }), {});

export {
  emailValidationPattern,
  isFormValidPropName,
  formConfigToFormState,
  formConfigToFormView,
  updateFormValidationState,
  updateFormFieldState,
  formStateToSubmitData
}
