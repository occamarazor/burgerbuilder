const updateState = (state, updatedSlice) => ({ ...state, ...updatedSlice });

const ordersObjToArr = (orders) => {
  return Object.entries(orders).reduce((arr, [ id, obj ]) => [
    ...arr,
    { id, ...obj }
  ], [])
};

const millisecondsToDate = (milsecs) => {
  const day = new Date(milsecs).getDate().toString().length === 1
    ? `0${new Date(milsecs).getDate()}`
    : new Date(milsecs).getDate();
  const month = new Date(milsecs).getMonth().toString().length === 1
    ? `0${new Date(milsecs).getMonth()}`
    : new Date(milsecs).getMonth();
  const year = new Date(milsecs).getFullYear();
  return `${day}.${month}.${year}`;
};

const millisecondsToDateObj = (milsecs) => new Date(milsecs);

const secondsToMilliseconds = (seconds) => seconds * 1000;

const millisecondsToSeconds = (milsecs) => milsecs / 1000;

const capitalize = (str) => str.charAt(0).toUpperCase() + str.slice(1);

// TODO: remove joiner
const joiner = (...args) => args.join(' ');

export {
  updateState,
  ordersObjToArr,
  millisecondsToDate,
  millisecondsToDateObj,
  secondsToMilliseconds,
  millisecondsToSeconds,
  capitalize,
  joiner
}