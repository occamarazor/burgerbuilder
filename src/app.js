import React                              from 'react';
import { ProductNavigation, ProductMain } from './features';

const App = () => (
  <>
    <ProductNavigation/>
    <ProductMain/>
  </>
);

export default App;
