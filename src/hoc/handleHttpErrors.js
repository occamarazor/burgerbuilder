import React, { PureComponent, Fragment } from 'react';
import { Modal }                          from '../ui'

// handleHttpErrors used in 1 place, left for demo purpose!
const handleHttpErrors = (WrappedComponent, http) => (
  class extends PureComponent {
    constructor(props) {
      super(props);
      
      this.state = {
        errorMessage: null
      };
      
      // Create in constructor or CWM: interceptors needed before WrappedComponent rendering
      this.reqInterceptors = http.interceptors.request.use(
        (requestConfig) => {
          // clear any error when sending new request
          this.setState({ errorMessage: null });
          return requestConfig;
        }
      );
      
      this.resInterceptors = http.interceptors.response.use(
        (responseConfig) => responseConfig,
        (responseError) => {
          this.setState({ errorMessage: responseError.message });
        }
      );
    };
    
    componentWillUnmount() {
      http.interceptors.request.eject(this.reqInterceptors);
      http.interceptors.response.eject(this.resInterceptors);
    };
    
    errorDismissHandler = () => {
      this.setState({ errorMessage: null });
    };
    
    render() {
      return (
        <Fragment>
          <WrappedComponent {...this.props}/>
          <Modal isVisible={Boolean(this.state.errorMessage)} onClose={this.errorDismissHandler}>
            <h3>{this.state.errorMessage}</h3>
          </Modal>
        </Fragment>
      );
    };
  }
);

export default handleHttpErrors;