import React from 'react';
import { shallow } from 'enzyme';

import App from './app';
import { ProductNavigation, ProductMain } from './features';

describe('<App />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<App />);
  });

  it('should render without crashing', () => {
    shallow(<App />);
  });

  it('should render one <ProductNavigation /> element', () => {
    expect(wrapper.find(ProductNavigation)).toHaveLength(1);
  });

  it('should render one <ProductMain /> element', () => {
    expect(wrapper.find(ProductMain)).toHaveLength(1);
  });
});
