const CHECKOUT_FORM_HEADER = 'Enter your contact data';

const CHECKOUT_FORM_NAME = 'CheckoutForm';

const CHECKOUT_FORM_CONFIG = {
  name:           {
    element: 'input',
    config:  {
      type:        'text',
      label:       'name',
      placeholder: 'Your name',
      validation:  {
        required: true
      }
    }
  },
  address:        {
    element: 'textarea',
    config:  {
      label:       'address',
      placeholder: 'Your address',
      validation:  {
        required: true
      }
    }
  },
  zipCode:        {
    element: 'input',
    config:  {
      type:        'text',
      label:       'zip code',
      placeholder: 'Your zip code',
      validation:  {
        required:  true,
        minLength: 6,
        maxLength: 6
      }
    }
  },
  country:        {
    element: 'input',
    config:  {
      type:         'text',
      label:        'country',
      placeholder:  'Your country',
      defaultValue: 'Russia',
      validation:   {
        required: true
      }
    }
  },
  city:        {
    element: 'input',
    config:  {
      type:         'text',
      label:        'city',
      placeholder:  'Your city',
      validation:   {
        required: true
      }
    }
  },
  deliveryMethod: {
    element: 'select',
    config:  {
      label:        'delivery',
      placeholder:  'Choose delivery method',
      defaultValue: 'fast',
      options:      [
        { value: 'fast', label: 'fastest' },
        { value: 'cheap', label: 'cheapest' },
        { value: 'reliable', label: 'most reliable' }
      ]
    }
  }
};

const CHECKOUT_FORM_SUBMIT_BUTTON = {
  label: 'ORDER',
  css:   [
    'Controls',
    'OrderProduct'
  ],
  click: 'checkoutFormSubmitHandler'
};

const REQUEST_ERROR_LABEL = 'Order can\'t be sent!';

export {
  CHECKOUT_FORM_HEADER,
  CHECKOUT_FORM_NAME,
  CHECKOUT_FORM_CONFIG,
  CHECKOUT_FORM_SUBMIT_BUTTON,
  REQUEST_ERROR_LABEL
};