// LIBS
import React, { PureComponent }        from 'react';
import { connect }                     from 'react-redux';
import PropTypes                       from 'prop-types';
// COMMON
import { http, routerPaths }           from '../../../../configs';
import {
  isFormValidPropName,
  formConfigToFormState,
  formConfigToFormView,
  updateFormValidationState,
  updateFormFieldState,
  formStateToSubmitData
}                                      from '../../../../helpers';
import { Alert, Button }               from '../../../../ui';
import { mainActions, builderActions } from '../../../actions';
import { loginSelectors }              from '../../../selectors';
// COMPONENT
import {
  CHECKOUT_FORM_HEADER,
  CHECKOUT_FORM_NAME,
  CHECKOUT_FORM_CONFIG,
  CHECKOUT_FORM_SUBMIT_BUTTON,
  REQUEST_ERROR_LABEL
}                                      from './product-checkout-form-constants';
import styles                          from './product-checkout-form.module.css';

// TODO: ref to focus on 1st form field
class ProductCheckoutFormContainer extends PureComponent {
  state = formConfigToFormState(CHECKOUT_FORM_NAME, CHECKOUT_FORM_CONFIG);
  
  componentDidMount() {
    const { history, productIngredients } = this.props;
    if(productIngredients.length <= 0) {
      history.replace(routerPaths.ROOT);
    }
  }
  
  // TODO: remove, each container must have it's own local instance
  componentWillUnmount() {
    const { isRequestError, onRequestErrorUpdate } = this.props;
    if(isRequestError) {
      onRequestErrorUpdate({ isRequestError: false });
    }
  }
  
  fieldChangeHandler = (fieldName, fieldValue) => {
    this.setState((formState) => updateFormFieldState(
      CHECKOUT_FORM_NAME,
      CHECKOUT_FORM_CONFIG,
      formState,
      fieldName,
      fieldValue)
    );
    
    this.setState((formState) => updateFormValidationState(CHECKOUT_FORM_NAME, formState));
  };
  
  checkoutFormSubmitHandler = (event) => {
    event.preventDefault();
    
    const {
      history,
      productIngredients,
      productPrice,
      onDataLoaderUpdate,
      onRequestErrorUpdate,
      onProductIngredientsReset,
      authentication: { authToken, userId, email }
    } = this.props;
    
    const checkoutFormData = formStateToSubmitData(CHECKOUT_FORM_NAME, this.state);
    
    const checkoutFormOrder = {
      date: Date.now(),
      ingredients: productIngredients,
      price: productPrice,
      customerId: userId,
      customer: {
        email,
        ...checkoutFormData
      }
    };
  
    onDataLoaderUpdate({ isDataLoading: true });
  
    // TODO: move request to redux
    // TODO: Promise to async await
    // TODO: slow network imitation
    setTimeout(() => {
      http
        .app
        .post(`/orders.json?auth=${authToken}`, checkoutFormOrder)
        .then(({ status }) => {
          if(status === 200) {
            onDataLoaderUpdate({ isDataLoading: false });
            onProductIngredientsReset();
            history.replace(routerPaths.ROOT);
          }
        })
        .catch((error) => {
          console.log(error);
          onDataLoaderUpdate({ isDataLoading: false });
          onRequestErrorUpdate({ isRequestError: true });
        });
    }, 1000);
  };
  
  render() {
    const { isRequestError } = this.props;
    
    const checkoutFormView = formConfigToFormView(
      CHECKOUT_FORM_NAME,
      CHECKOUT_FORM_CONFIG,
      this.state,
      this.fieldChangeHandler
    );
    
    const isCheckoutFormValid = this.state[isFormValidPropName(CHECKOUT_FORM_NAME)];
    
    return isRequestError
      ? <Alert>{REQUEST_ERROR_LABEL}</Alert>
      : (
        <div className={styles.Form}>
          <h3 className={styles.FormHeader}>{CHECKOUT_FORM_HEADER}</h3>
          <form>
            
            {checkoutFormView}
            
            <Button classNames={CHECKOUT_FORM_SUBMIT_BUTTON.css} disable={!isCheckoutFormValid}
                    click={this[CHECKOUT_FORM_SUBMIT_BUTTON.click]}>
              {CHECKOUT_FORM_SUBMIT_BUTTON.label}
            </Button>
          </form>
        </div>
      );
  }
}

// TODO: collect all proptypes in 1 place
ProductCheckoutFormContainer.propTypes = {
  isRequestError: PropTypes.bool.isRequired,
  productIngredients: PropTypes.arrayOf(
    PropTypes.exact({
      type: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired
    }).isRequired
  ).isRequired,
  productPrice: PropTypes.number.isRequired,
  onDataLoaderUpdate: PropTypes.func.isRequired,
  onRequestErrorUpdate: PropTypes.func.isRequired,
  onProductIngredientsReset: PropTypes.func.isRequired,
  authentication: PropTypes.exact({
    userId: PropTypes.string,
    email: PropTypes.string,
    authToken: PropTypes.string,
    refreshToken: PropTypes.string
  }).isRequired
};

// TODO: use selectors
const mapStateToProps = (state) => ({
  isRequestError: state.mainReducer.isRequestError,
  productIngredients: state.builderReducer.productIngredients,
  productPrice: state.builderReducer.productPrice,
  authentication: loginSelectors.getAuthentication(state)
});

// TODO: use bindActionCreators
const mapDispatchToProps = (dispatch) => ({
  onDataLoaderUpdate: (payload) => dispatch(mainActions.updateDataLoader(payload)),
  onRequestErrorUpdate: (payload) => dispatch(mainActions.updateRequestError(payload)),
  onProductIngredientsReset: () => dispatch(builderActions.resetProductIngredients())
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductCheckoutFormContainer);
