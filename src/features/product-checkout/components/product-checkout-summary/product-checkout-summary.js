import React, { Component }                 from 'react';
import PropTypes                            from 'prop-types';
import { ProductIngredients, ProductPrice } from '../../../../common';
import { Button }                           from '../../../../ui';
import { HEADER_LABEL, BUTTONS }            from './product-checkout-summary-constants';
import styles                               from './product-checkout-summary.module.css';

class ProductCheckoutSummary extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return JSON.stringify(this.props.productIngredients) !== JSON.stringify(nextProps.productIngredients)
      || this.props.productPrice !== nextProps.productPrice;
  };
  
  render() {
    const { productIngredients, productPrice } = this.props;
    const checkoutButtons = BUTTONS.map((b) => (
      <Button key={b.label}
              classNames={b.css}
              click={this.props[b.click]}>
        {b.label}
      </Button>)
    );
    
    return (
      <div className={styles.CheckoutSummary}>
        <h2 className={styles.CheckoutSummaryHeader}>{HEADER_LABEL}</h2>
        <ProductIngredients productIngredients={productIngredients}/>
        <ProductPrice productPrice={productPrice}/>
        <div className={styles.CheckoutSummaryButtons}>
          {checkoutButtons}
        </div>
      </div>
    );
  };
}

// TODO: collect all proptypes in 1 place
ProductCheckoutSummary.propTypes = {
  productIngredients:    PropTypes.arrayOf(
    PropTypes.exact({
      type:  PropTypes.string.isRequired,
      price: PropTypes.number.isRequired
    }).isRequired
  ).isRequired,
  productPrice:          PropTypes.number.isRequired,
  onCheckoutCancelBtn:   PropTypes.func.isRequired,
  onCheckoutContinueBtn: PropTypes.func.isRequired
};

export default ProductCheckoutSummary;