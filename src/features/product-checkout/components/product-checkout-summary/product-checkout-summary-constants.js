const HEADER_LABEL = 'We hope it tastes well!';

const BUTTONS = [
  {
    label: 'CANCEL',
    css:   [ 'Cancel' ],
    click: 'onCheckoutCancelBtn'
  },
  {
    label: 'CONTINUE',
    css:   [ 'Continue' ],
    click: 'onCheckoutContinueBtn'
  }
];

export {
  HEADER_LABEL,
  BUTTONS,
};