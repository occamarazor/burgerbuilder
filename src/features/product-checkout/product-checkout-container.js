// LIBS
import React, { PureComponent, Fragment } from 'react';
import { connect }                        from 'react-redux';
import PropTypes                          from 'prop-types';
// COMMON
import { routerPaths }                    from '../../configs';
import { RouteWithSubRoutes }             from '../../common';
// COMPONENT
  import { ProductCheckoutSummary }         from './components';

class ProductCheckoutContainer extends PureComponent {
  componentDidMount() {
    const { history, productIngredients } = this.props;
    if(productIngredients.length <= 0) {
      history.replace(routerPaths.ROOT);
    }
  }
  
  checkoutCancelHandler = () => {
    this.props.history.goBack();
  };
  
  checkoutContinueHandler = () => {
    const { location, history } = this.props;
    if(location.pathname !== routerPaths.CHECKOUT_FORM) {
      history.push(routerPaths.CHECKOUT_FORM);
    }
  };
  
  render() {
    const { routes, productIngredients, productPrice } = this.props;
    
    return (
      <Fragment>
        <ProductCheckoutSummary
          productIngredients={productIngredients}
          productPrice={productPrice}
          onCheckoutCancelBtn={this.checkoutCancelHandler}
          onCheckoutContinueBtn={this.checkoutContinueHandler}/>
        
        {routes.map((route, i) => (
          <RouteWithSubRoutes key={i} {...route} />
        ))}
      </Fragment>
    );
  };
}

// TODO: collect all proptypes in 1 place
ProductCheckoutContainer.propTypes = {
  routes: PropTypes.arrayOf(
    PropTypes.exact({
      path: PropTypes.string.isRequired,
      exact: PropTypes.bool,
      component: PropTypes.elementType.isRequired
    }).isRequired
  ).isRequired,
  productIngredients: PropTypes.arrayOf(
    PropTypes.exact({
      type: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired
    }).isRequired
  ).isRequired,
  productPrice: PropTypes.number.isRequired
};

// TODO: use selectors
const mapStateToProps = (state) => ({
  productIngredients: state.builderReducer.productIngredients,
  productPrice: state.builderReducer.productPrice
});

export default connect(mapStateToProps)(ProductCheckoutContainer);