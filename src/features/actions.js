import * as mainActions    from './product-main/product-main-actions';
import * as builderActions from './product-builder/product-builder-actions';
import * as ordersActions  from './product-orders/product-orders-actions';
import * as loginActions   from './product-login/product-login-actions';

export {
  mainActions,
  builderActions,
  ordersActions,
  loginActions
};