import React, { PureComponent } from 'react';
import { connect }              from 'react-redux';
import PropTypes                from 'prop-types';

import { loginSelectors } from '../selectors';

import { ProductNavigationMain, ProductNavigationSide } from './components';
import { INITIAL_STATE }                                from './product-navigation-constants';

class ProductNavigationContainer extends PureComponent {
  state = INITIAL_STATE;
  
  componentDidMount() {
    window.addEventListener('resize', this.mobileModeToggleHandler);
    this.mobileModeToggleHandler();
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.mobileModeToggleHandler);
  }
  
  mobileModeToggleHandler = () => {
    this.setState({ isMobileModeActive: window.innerWidth < 500 });
  };
  
  sideNavToggleHandler = () => {
    this.setState((state) => ({
      isSideNavOpen: !state.isSideNavOpen
    }));
  };
  
  render() {
    const { isMobileModeActive, isSideNavOpen } = this.state;
    const { isAuthenticated } = this.props;
    
    return (
      <header>
        <ProductNavigationMain isMobileModeActive={isMobileModeActive}
                               onSideNavBtnClick={this.sideNavToggleHandler}
                               isAuthenticated={isAuthenticated}/>
        {isMobileModeActive
          ? (
            <ProductNavigationSide
              isVisible={isSideNavOpen && isMobileModeActive}
              onSideNavClose={this.sideNavToggleHandler}
              isAuthenticated={isAuthenticated}/>
          )
          : null
        }
      </header>
    );
  }
}

// TODO: collect all proptypes in 1 place
ProductNavigationContainer.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
  isAuthenticated: loginSelectors.getAuthenticationToken(state) !== null
});

export default connect(mapStateToProps)(ProductNavigationContainer);