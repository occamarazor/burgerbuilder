const INITIAL_STATE = {
  isMobileModeActive: false,
  isSideNavOpen:      false
};

export {
  INITIAL_STATE
}