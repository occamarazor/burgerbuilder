import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import styles from './product-navigation-item.module.css';

const ProductNavigationItem = ({ path, exact, click, children }) => {
  return (
    <li className={styles.NavItem}>
      <NavLink to={path} exact={exact} activeClassName={styles.Active} onClick={click}>
        {children}
      </NavLink>
    </li>
  );
};

// TODO: collect all proptypes in 1 place
ProductNavigationItem.propTypes = {
  path: PropTypes.string.isRequired,
  exact: PropTypes.bool.isRequired,
  click: PropTypes.func,
  children: PropTypes.string.isRequired,
};

export default ProductNavigationItem;
