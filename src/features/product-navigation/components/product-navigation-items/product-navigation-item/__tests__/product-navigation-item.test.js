import React from 'react';
import { NavLink } from 'react-router-dom';
import { shallow } from 'enzyme';

import ProductNavigationItem from '../product-navigation-item';
import styles from '../product-navigation-item.module.css';

describe('<ProductNavigationItem />', () => {
  let props;

  beforeEach(() => {
    props = {
      path: '/',
      exact: true,
      click: () => {},
      children: 'Builder',
    };
  });

  it('should render without crashing', () => {
    shallow(<ProductNavigationItem {...props} />);
  });

  it('should render li with NavLink', () => {
    const wrapper = shallow(<ProductNavigationItem {...props} />);
    const { path, exact, click, children } = props;
    const navItem = (
      <li className={styles.NavItem}>
        <NavLink to={path} exact={exact} activeClassName={styles.Active} onClick={click}>
          {children}
        </NavLink>
      </li>
    );

    expect(wrapper.contains(navItem)).toEqual(true);
  });
});
