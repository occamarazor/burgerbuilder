import React, { PureComponent } from 'react';
import PropTypes                from 'prop-types';
import {
  AUTHORIZED_NAV_ITEMS,
  UNAUTHORIZED_NAV_ITEMS
}                               from './product-navigation-items-constants';
import { navItemsFromConfig }   from './product-navigation-items-utils';
import styles                   from './product-navigation-items.module.css';

class ProductNavigationItems extends PureComponent {
  render() {
    const { isAuthenticated, onNavItemClick } = this.props;
    
    return (
      <nav>
        <ul className={styles.NavItems}>
          {isAuthenticated
            ? navItemsFromConfig(AUTHORIZED_NAV_ITEMS, onNavItemClick)
            : navItemsFromConfig(UNAUTHORIZED_NAV_ITEMS, onNavItemClick)
          }
        </ul>
      </nav>
    );
  }
}

// TODO: collect all proptypes in 1 place
ProductNavigationItems.propTypes = {
  onNavItemClick: PropTypes.func,
  isAuthenticated: PropTypes.bool.isRequired
};

export default ProductNavigationItems;
