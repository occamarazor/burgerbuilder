import React                     from 'react';
import { ProductNavigationItem } from '../index';

export const navItemsFromConfig = (items, onNavItemClick) => (
  items.map((item) => (
    <ProductNavigationItem
      key={item.label}
      path={item.path}
      exact={item.exact}
      click={onNavItemClick}>
      {item.label}
    </ProductNavigationItem>
  ))
);
