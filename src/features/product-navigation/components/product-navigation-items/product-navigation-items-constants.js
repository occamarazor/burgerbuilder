import { routerPaths } from '../../../../configs';

const LOGIN_NAV_ITEM_CONFIG = { label: 'Login', path: routerPaths.LOGIN, exact: true };
const LOGOUT_NAV_ITEM_CONFIG = { label: 'Logout', path: routerPaths.LOGOUT, exact: true };
const BUILDER_NAV_ITEM_CONFIG = { label: 'Builder', path: routerPaths.ROOT, exact: true };
const ORDERS_NAV_ITEM_CONFIG = { label: 'Orders', path: routerPaths.ORDERS, exact: true };

const AUTHORIZED_NAV_ITEMS = [
  BUILDER_NAV_ITEM_CONFIG,
  ORDERS_NAV_ITEM_CONFIG,
  LOGOUT_NAV_ITEM_CONFIG
];

const UNAUTHORIZED_NAV_ITEMS = [
  BUILDER_NAV_ITEM_CONFIG,
  LOGIN_NAV_ITEM_CONFIG
];

export {
  AUTHORIZED_NAV_ITEMS,
  UNAUTHORIZED_NAV_ITEMS
}
