// LIBS
import React, { PureComponent }                           from 'react';
import PropTypes                                          from 'prop-types';
// COMMON
import { Button }                                         from '../../../../ui';
// COMPONENT
import { ProductNavigationBrand, ProductNavigationItems } from '../';
import { SIDENAV_BUTTON }                                 from './product-navigation-main-constants';
import styles                                             from './product-navigation-main.module.css';

class ProductNavigationMain extends PureComponent {
  render() {
    const { isMobileModeActive, onSideNavBtnClick, isAuthenticated } = this.props;
    
    return (
      <div className={styles.MainNav}>
        <div className={styles.BrandWrapper}>
          <ProductNavigationBrand/>
        </div>
        {isMobileModeActive
          ? <Button classNames={SIDENAV_BUTTON.css} click={onSideNavBtnClick}/>
          : <ProductNavigationItems isAuthenticated={isAuthenticated}/>
        }
      </div>
    );
  }
}

// TODO: collect all proptypes in 1 place
ProductNavigationMain.propTypes = {
  isMobileModeActive: PropTypes.bool.isRequired,
  onSideNavBtnClick: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};

export default ProductNavigationMain;