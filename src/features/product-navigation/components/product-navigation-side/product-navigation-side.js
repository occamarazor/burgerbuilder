// LIBS
import React, { PureComponent, Fragment }                     from 'react';
import PropTypes                                          from 'prop-types';
// COMMON
import { joiner }                                         from '../../../../helpers';
import { Backdrop }                                       from '../../../../ui';
// COMPONENT
import { ProductNavigationBrand, ProductNavigationItems } from '../';
import styles                                             from './product-navigation-side.module.css';

class ProductNavigationSide extends PureComponent {
  render() {
    const { isVisible, onSideNavClose, isAuthenticated } = this.props;
    const sideNavStyles = isVisible
      ? joiner(styles.SideNav, styles.Visible)
      : joiner(styles.SideNav, styles.Hidden);
    
    return (
      <Fragment>
        <div className={sideNavStyles}>
          <div className={styles.BrandWrapper}>
            <ProductNavigationBrand onNavBrandClick={onSideNavClose}/>
          </div>
          <ProductNavigationItems onNavItemClick={onSideNavClose}
                                  isAuthenticated={isAuthenticated}/>
        </div>
        <Backdrop isVisible={isVisible} click={onSideNavClose}/>
      </Fragment>
    );
  }
}

// TODO: collect all proptypes in 1 place
ProductNavigationSide.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  onSideNavClose: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};

export default ProductNavigationSide;