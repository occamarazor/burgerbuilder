const BRAND_NAME = 'BRG';
const IMG_CAPTION = 'Burger Builder Logo';

export {
  BRAND_NAME,
  IMG_CAPTION
}