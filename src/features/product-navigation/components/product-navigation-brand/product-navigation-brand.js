import React, { PureComponent }    from 'react';
import { NavLink }                 from 'react-router-dom';
import PropTypes                   from 'prop-types';
import { routerPaths }             from '../../../../configs';
import logo                        from '../../../../assets/images/logo.png';
import { BRAND_NAME, IMG_CAPTION } from './product-navigation-brand-constants';
import styles                      from './product-navigation-brand.module.css';

class ProductNavigationBrand extends PureComponent {
  render() {
    return (
      <NavLink to={routerPaths.ROOT} className={styles.Brand} onClick={this.props.onNavBrandClick}>
        <img src={logo} alt={IMG_CAPTION}/>
        <h3>{BRAND_NAME}</h3>
      </NavLink>
    );
  }
}

// TODO: collect all proptypes in 1 place
ProductNavigationBrand.propTypes = {
  onNavBrandClick: PropTypes.func
};

export default ProductNavigationBrand;