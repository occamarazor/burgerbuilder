// LIBS
import React, { Component }   from 'react';
import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';
import PropTypes              from 'prop-types';
// COMMON
import { loginActions }       from '../actions';

class ProductLogoutContainer extends Component {
  componentDidMount() {
    const { history, onUserAuthenticationDrop } = this.props;
    onUserAuthenticationDrop(history);
  }
  
  render() {
    return (
      <></>
    );
  };
}

ProductLogoutContainer.propTypes = {
  onUserAuthenticationDrop: PropTypes.func.isRequired
};

const mapDispatchToProps = (dispatch) => ({
  onUserAuthenticationDrop: bindActionCreators(loginActions.dropUserAuthentication, dispatch)
});

export default connect(null, mapDispatchToProps)(ProductLogoutContainer);
