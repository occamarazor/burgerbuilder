import { UPDATE_DATA_LOADER, UPDATE_REQUEST_ERROR } from './product-main-constants';

const updateDataLoader = (payload) => ({
  type: UPDATE_DATA_LOADER,
  payload
});

const updateRequestError = (payload) => ({
  type: UPDATE_REQUEST_ERROR,
  payload
});

export {
  updateDataLoader,
  updateRequestError
};