import { UPDATE_DATA_LOADER, UPDATE_REQUEST_ERROR } from './product-main-constants';
import { updateState }                              from '../../helpers';

const INITIAL_STATE = {
  isDataLoading:  false,
  isRequestError: false
};

const updateLoader = (prevState, action) => updateState(prevState, {
  isDataLoading: action.payload.isDataLoading
});

const updateError = (prevState, action) => updateState(prevState, {
  isRequestError: action.payload.isRequestError
});

const productMainReducer = (prevState = INITIAL_STATE, action) => {
  switch(action.type) {
    case UPDATE_DATA_LOADER:
      return updateLoader(prevState, action);
    case UPDATE_REQUEST_ERROR:
      return updateError(prevState, action);
    default:
      return prevState;
  }
};

export default productMainReducer;