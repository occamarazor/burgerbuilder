// LIBS
import React, { PureComponent, Fragment } from 'react';
import { bindActionCreators }             from 'redux';
import { connect }                        from 'react-redux';
import { Switch, withRouter }             from 'react-router-dom'
import PropTypes                          from 'prop-types';
// COMMON
import { http, appRoutes }                from '../../configs';
import { handleHttpErrors }               from '../../hoc';
import { RouteWithSubRoutes }             from '../../common';
import { Spinner }                        from '../../ui';
import { loginActions }                   from '../actions';
import { loginSelectors }                 from '../selectors';
// COMPONENT
import styles                             from './product-main.module.css';

class ProductMainContainer extends PureComponent {
  componentDidMount() {
    const { history, onUserAuthentication } = this.props;
    onUserAuthentication(history);
  }
  
  render() {
    const { isDataLoading, isAuthenticated } = this.props;
    const routes = isAuthenticated ? appRoutes.authorizedRoutes : appRoutes.unauthorizedRoutes;
    
    return (
      <Fragment>
        <main className={styles.Main}>
          <Switch>
            {routes.map((route, i) => (
              <RouteWithSubRoutes key={i} {...route} />
            ))}
          </Switch>
        </main>
        {isDataLoading ? <Spinner/> : null}
      </Fragment>
    );
  };
}

// TODO: collect all proptypes in 1 place
ProductMainContainer.propTypes = {
  isDataLoading: PropTypes.bool.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};

// TODO: use selectors
const mapStateToProps = (state) => ({
  isDataLoading: state.mainReducer.isDataLoading,
  isAuthenticated: loginSelectors.getAuthenticationToken(state) !== null
});

const mapDispatchToProps = (dispatch) => ({
  onUserAuthentication: bindActionCreators(loginActions.checkUserAuthentication, dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(handleHttpErrors(ProductMainContainer, http.app)));