import ProductNavigationContainer from './product-navigation/product-navigation-container';
import ProductMainContainer       from './product-main/product-main-container';

export {
  ProductNavigationContainer as ProductNavigation,
  ProductMainContainer as ProductMain
};