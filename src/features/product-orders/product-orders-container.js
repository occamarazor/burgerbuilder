// LIBS
import React, { PureComponent } from 'react';
import { connect }              from 'react-redux';
import PropTypes                from 'prop-types';
// COMMON
import { Alert }                from '../../ui';
import { ordersActions }        from '../actions';
import { loginSelectors }       from '../selectors';
// COMPONENT
import {
  REQUEST_ERROR_LABEL,
  ORDERS_EMPTY_LABEL
}                               from './product-orders-constants';
import { ProductOrdersItem }    from './components';
import { bindActionCreators }   from 'redux';

class ProductOrdersContainer extends PureComponent {
  componentDidMount() {
    const { authToken, userId, onOrdersRequest } = this.props;
    onOrdersRequest(authToken, userId);
  }
  
  render() {
    const { orders, requestError, isDataLoading } = this.props;
    
    // TODO: move to helpers, optimize
    let ordersPage = orders.map((o) => {
      const ingredients = o.ingredients.reduce((arr, pi) => {
        const ingTypeIndex = arr.findIndex((ing) => ing.type === pi.type);
        
        if(ingTypeIndex !== -1) {
          arr[ingTypeIndex] = {
            type: pi.type,
            amount: ++arr[ingTypeIndex].amount,
            price: arr[ingTypeIndex].price + pi.price
          };
          return arr;
        }
        
        return [ ...arr, { type: pi.type, amount: 1, price: pi.price } ];
      }, []);
      
      return (
        <ProductOrdersItem key={o.id}
                           id={o.id}
                           date={o.date}
                           orderIngredients={ingredients}
                           price={o.price}
                           customer={o.customer}/>
      );
    });
    
    // TODO: requestError handling on route change
    if(requestError) {
      ordersPage = <Alert>{REQUEST_ERROR_LABEL}</Alert>;
    } else if(!isDataLoading && orders.length <= 0) {
      ordersPage = <Alert>{ORDERS_EMPTY_LABEL}</Alert>;
    }
    return ordersPage;
  };
}

// TODO: collect all proptypes in 1 place
ProductOrdersContainer.propTypes = {
  orders: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.string.isRequired,
      date: PropTypes.number.isRequired,
      ingredients: PropTypes.arrayOf(
        PropTypes.exact({
          type: PropTypes.string.isRequired,
          price: PropTypes.number.isRequired
        }).isRequired
      ).isRequired,
      price: PropTypes.number.isRequired,
      customer: PropTypes.exact({
        address: PropTypes.string.isRequired,
        city: PropTypes.string.isRequired,
        country: PropTypes.string.isRequired,
        deliveryMethod: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        zipCode: PropTypes.string.isRequired
      }).isRequired,
      customerId: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  requestError: PropTypes.string.isRequired,
  authToken: PropTypes.string,
  userId: PropTypes.string,
  isDataLoading: PropTypes.bool.isRequired,
  onOrdersRequest: PropTypes.func.isRequired
};

// TODO: use selectors
const mapStateToProps = (state) => ({
  orders: state.ordersReducer.orders,
  requestError: state.ordersReducer.requestError,
  authToken: loginSelectors.getAuthenticationToken(state),
  userId: loginSelectors.getUserId(state),
  isDataLoading: state.mainReducer.isDataLoading
});

// TODO: use bindActionCreators
const mapDispatchToProps = (dispatch) => ({
  onOrdersRequest: bindActionCreators(ordersActions.requestOrders, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductOrdersContainer);