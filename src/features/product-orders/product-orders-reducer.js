import { updateState } from '../../helpers';

import {
  INITIAL_STATE,
  REQUEST_ORDERS_SUCCESS,
  REQUEST_ORDERS_ERROR
} from './product-orders-constants';

const setOrdersData = (state, orders) =>
  updateState(state, { orders });

const setOrdersError = (state, requestError) =>
  updateState(state, { requestError });

const productOrdersReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case REQUEST_ORDERS_SUCCESS:
      return setOrdersData(state, action.orders);
    case REQUEST_ORDERS_ERROR:
      return setOrdersError(state, action.requestError);
    default:
      return state;
  }
};

export default productOrdersReducer;
