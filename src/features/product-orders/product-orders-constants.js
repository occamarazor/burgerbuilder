const INITIAL_STATE = {
  // Dummy data for a nice look while data is loading
  orders: [
    {
      id: 'AAA',
      customer: {
        address: 'Test Street 1, apt. 1',
        city: 'Moscow',
        country: 'Russia',
        deliveryMethod: 'fast',
        email: 'testone@test.com',
        name: 'Test One',
        zipCode: '111111'
      },
      customerId: 'XXX',
      date: 1572272692716,
      ingredients: [
        { price: 0.5, type: 'egg' },
        { price: 0.2, type: 'potato' },
        { price: 0.9, type: 'meat' },
        { price: 0.4, type: 'tomato' }
      ],
      price: 2
    },
    {
      id: 'BBB',
      customer: {
        address: 'Test Street 2, apt. 2',
        city: 'New York',
        country: 'USA',
        deliveryMethod: 'reliable',
        email: 'testtwo@test.com',
        name: 'Test Two',
        zipCode: '222222'
      },
      customerId: 'YYY',
      date: 1572272791427,
      ingredients: [
        { price: 0.5, type: 'egg' },
        { price: 0.2, type: 'potato' },
        { price: 0.9, type: 'meat' },
        { price: 0.4, type: 'tomato' },
        { price: 0.5, type: 'egg' },
        { price: 0.2, type: 'potato' },
        { price: 0.9, type: 'meat' },
        { price: 0.4, type: 'tomato' }
      ],
      price: 4
    },
    {
      id: 'CCC',
      customer: {
        address: 'Test Street 3, apt. 3',
        city: 'Delhi',
        country: 'India',
        deliveryMethod: 'cheap',
        email: 'testthree@test.com',
        name: 'Test Three',
        zipCode: '333333'
      },
      customerId: 'ZZZ',
      date: 1572273706069,
      ingredients: [
        { price: 0.5, type: 'egg' },
        { price: 0.2, type: 'potato' },
        { price: 0.9, type: 'meat' },
        { price: 0.4, type: 'tomato' }
      ],
      price: 2
    }
  ],
  requestError: ''
};

const REQUEST_ERROR_LABEL = 'Error: orders can\'t be loaded!';

const ORDERS_EMPTY_LABEL = 'Orders are empty';

const REQUEST_ORDERS = 'REQUEST_ORDERS';
const REQUEST_ORDERS_SUCCESS = 'REQUEST_ORDERS_SUCCESS';
const REQUEST_ORDERS_ERROR = 'REQUEST_ORDERS_ERROR';

export {
  INITIAL_STATE,
  REQUEST_ERROR_LABEL,
  ORDERS_EMPTY_LABEL,
  REQUEST_ORDERS,
  REQUEST_ORDERS_SUCCESS,
  REQUEST_ORDERS_ERROR
}
