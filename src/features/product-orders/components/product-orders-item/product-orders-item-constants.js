const ID_LABEL = 'Order #:';

const DATE_LABEL = 'Ordered on:';

const CUSTOMER_NAME_LABEL = 'Name:';

const CUSTOMER_EMAIL_LABEL = 'Email:';

const ADDRESS_LABEL = 'Delivery address:';

const TABLE_LABELS = [ 'Ingredient', 'Amount', 'Price' ];

export {
  ID_LABEL,
  DATE_LABEL,
  CUSTOMER_NAME_LABEL,
  CUSTOMER_EMAIL_LABEL,
  ADDRESS_LABEL,
  TABLE_LABELS
}