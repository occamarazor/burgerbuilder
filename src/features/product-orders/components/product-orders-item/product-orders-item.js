import React                  from 'react';
import PropTypes              from 'prop-types';
import { millisecondsToDate } from '../../../../helpers';
import {
  ID_LABEL,
  DATE_LABEL,
  CUSTOMER_NAME_LABEL,
  CUSTOMER_EMAIL_LABEL,
  ADDRESS_LABEL,
  TABLE_LABELS
}                             from './product-orders-item-constants';
import styles                 from './product-orders-item.module.css';

const ProductOrdersItem = ({
  id,
  date,
  orderIngredients,
  price,
  customer: { email, name, country, city, address }
}) => {
  const headRow = (
    <tr>
      {TABLE_LABELS.map((tl) => <th key={tl}>{tl}</th>)}
    </tr>
  );
  
  const ingredientsRow = orderIngredients.map((pi, i) => (
    <tr key={i}>
      <td>{pi.type}</td>
      <td>{pi.amount}</td>
      <td>${pi.price.toFixed(2)}</td>
    </tr>
  ));
  
  const totalPriceRow = (
    <tr>
      <td><strong>TOTAL</strong></td>
      <td colSpan={2}>${price.toFixed(2)}</td>
    </tr>
  );
  
  return (
    <div className={styles.OrdersItem}>
      <div className={styles.OrdersItemInfo}>
        <p>{ID_LABEL} {id}</p>
        <p>{DATE_LABEL} {millisecondsToDate(date)}</p>
        <p>{CUSTOMER_NAME_LABEL} {name}</p>
        <p>{CUSTOMER_EMAIL_LABEL} {email}</p>
        <p>{ADDRESS_LABEL} {country}, {city}, {address}</p>
      </div>
      <table className={styles.OrdersItemTable}>
        <thead>
          {headRow}
        </thead>
        <tbody>
          {ingredientsRow}
          {totalPriceRow}
        </tbody>
      </table>
    </div>
  );
};

// TODO: collect all proptypes in 1 place
ProductOrdersItem.propTypes = {
  id: PropTypes.string.isRequired,
  date: PropTypes.number.isRequired,
  orderIngredients: PropTypes.arrayOf(
    PropTypes.exact({
      type: PropTypes.string.isRequired,
      amount: PropTypes.number.isRequired,
      price: PropTypes.number.isRequired
    }).isRequired
  ).isRequired,
  price: PropTypes.number.isRequired,
  customer: PropTypes.exact({
    address: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    deliveryMethod: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    zipCode: PropTypes.string.isRequired
  }).isRequired
};

export default ProductOrdersItem;