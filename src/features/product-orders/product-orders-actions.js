import { http }        from '../../configs';
import { mainActions } from '../actions';

import {
  REQUEST_ORDERS,
  REQUEST_ORDERS_SUCCESS,
  REQUEST_ORDERS_ERROR
}                         from './product-orders-constants';
import { ordersObjToArr } from '../../helpers';

const requestOrders = (authToken, userId) => (dispatch) => {
  dispatch(mainActions.updateDataLoader({ isDataLoading: true }));
  
  // TODO: acts as a placeholder
  dispatch(getOrders());
  
  // TODO: promise to async await
  // TODO: move all endpoints
  // TODO: slow network imitation
  setTimeout(() => {
    http
      .app
      .get(`/orders.json?auth=${authToken}&orderBy="customerId"&equalTo="${userId}"`)
      .then(({ data }) => {
        dispatch(requestOrdersSuccess(ordersObjToArr(data)));
      })
      .catch((error) => {
        console.dir(error);
        dispatch(requestOrdersError(error));
      })
      .finally(() => {
        dispatch(mainActions.updateDataLoader({ isDataLoading: false }));
      });
  }, 1000);
};

const getOrders = () => ({
  type: REQUEST_ORDERS
});

const requestOrdersSuccess = (orders) => ({
  type: REQUEST_ORDERS_SUCCESS,
  orders
});

const requestOrdersError = (error) => ({
  type: REQUEST_ORDERS_ERROR,
  requestError: error.response ? error.response.data.error.message : error.message
});

export {
  requestOrders
};
