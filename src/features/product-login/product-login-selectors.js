export const getAuthentication = (state) => state.loginReducer.authentication;

export const getAuthenticationToken = (state) => getAuthentication(state).authToken;

export const getUserId = (state) => getAuthentication(state).userId;
