import { millisecondsToDateObj, secondsToMilliseconds } from '../../helpers';

// TODO: merge to 1 consistent adapter
export const authenticationDataToState = (authenticationData) => ({
  userId: authenticationData.localId,
  email: authenticationData.email,
  authToken: authenticationData.idToken,
  refreshToken: authenticationData.refreshToken
  // expirationPeriod: authenticationData.expiresIn
});

export const authenticationDataToStorage = (authenticationData) => {
  const nowMilsecs = new Date().getTime();
  const tokenLifeMilsecs = secondsToMilliseconds(authenticationData.expiresIn);
  
  return {
    userId: authenticationData.localId,
    email: authenticationData.email,
    authToken: authenticationData.idToken,
    refreshToken: authenticationData.refreshToken,
    // expirationPeriod: authenticationData.expiresIn
    expirationDate: millisecondsToDateObj(nowMilsecs + tokenLifeMilsecs)
  };
};
