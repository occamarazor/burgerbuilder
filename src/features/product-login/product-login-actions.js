// TODO: routerHistory, handle separately
// TODO: localStorage,  handle separately

// COMMON
import { http, routerPaths } from '../../configs';
import {
  secondsToMilliseconds,
  millisecondsToSeconds
}                            from '../../helpers';
import { mainActions }       from '../actions';
// COMPONENT
import {
  LOCAL_STORAGE_AUTHENTICATION_ITEM,
  REQUEST_USER_AUTHENTICATION,
  REQUEST_USER_AUTHENTICATION_SUCCESS,
  REQUEST_USER_AUTHENTICATION_ERROR,
  DROP_USER_AUTHENTICATION,
  CHECK_USER_AUTHENTICATION_STATUS
}                            from './product-login-constants';
import {
  authenticationDataToStorage,
  authenticationDataToState
}                            from './product-login-adapter';

const requestUserAuthentication = (userCredentials, isSignUp, routerHistory) =>
  (dispatch) => {
    dispatch(mainActions.updateDataLoader({ isDataLoading: true }));
    
    // TODO: acts as a placeholder
    dispatch(getUserAuthentication());
    
    // TODO: envs get included in build
    const authRequestEndpoint = isSignUp ? 'signUp' : 'signInWithPassword';
    const authRequestUrl = `accounts:${authRequestEndpoint}?key=${process.env.REACT_APP_AUTH_API_KEY}`;
    const authRequestData = {
      ...userCredentials,
      returnSecureToken: true
    };
    
    // TODO: promise to async await
    // TODO: slow network imitation
    setTimeout(() => {
      http
        .auth
        .post(authRequestUrl, authRequestData)
        .then(({ data }) => {
          localStorage.setItem(
            LOCAL_STORAGE_AUTHENTICATION_ITEM,
            JSON.stringify(authenticationDataToStorage(data))
          );
          
          dispatch(requestUserAuthenticationSuccess(authenticationDataToState(data)));
          dispatch(startUserAuthenticationTimer(data.expiresIn, routerHistory));
        })
        .catch((error) => {
          console.dir(error);
          dispatch(requestUserAuthenticationError(error));
        })
        .finally(() => {
          dispatch(mainActions.updateDataLoader({ isDataLoading: false }));
        });
    }, 1000);
  };

const startUserAuthenticationTimer = (expirationTime, routerHistory) => (dispatch) => {
  setTimeout(() => {
    dispatch(dropUserAuthentication(routerHistory));
  }, secondsToMilliseconds(expirationTime));
};

const getUserAuthentication = () => ({
  type: REQUEST_USER_AUTHENTICATION
});

const requestUserAuthenticationSuccess = (authentication) => ({
  type: REQUEST_USER_AUTHENTICATION_SUCCESS,
  authentication
});

const requestUserAuthenticationError = (error) => ({
  type: REQUEST_USER_AUTHENTICATION_ERROR,
  requestError: error.response ? error.response.data.error.message : error.message
});

const dropUserAuthentication = (routerHistory) => {
  localStorage.removeItem(LOCAL_STORAGE_AUTHENTICATION_ITEM);
  routerHistory.replace(routerPaths.LOGIN);
  
  return {
    type: DROP_USER_AUTHENTICATION
  }
};

const checkUserAuthentication = (routerHistory) => (dispatch) => {
  const authentication = localStorage.getItem(LOCAL_STORAGE_AUTHENTICATION_ITEM);
  
  if(authentication) {
    const {
      userId,
      email,
      authToken,
      refreshToken,
      expirationDate
    } = JSON.parse(authentication);
    const now = new Date();
    const date = new Date(expirationDate);
    
    if(date > now) {
      const expirationTime = millisecondsToSeconds(date.getTime() - now.getTime());
      
      dispatch(requestUserAuthenticationSuccess({
        userId,
        email,
        authToken,
        refreshToken
      }));
      dispatch(startUserAuthenticationTimer(expirationTime, routerHistory));
    } else {
      dispatch(dropUserAuthentication(routerHistory));
    }
  }
  
  // TODO: not really necessary
  return {
    type: CHECK_USER_AUTHENTICATION_STATUS
  }
};

export {
  requestUserAuthentication,
  dropUserAuthentication,
  checkUserAuthentication
};