import { emailValidationPattern } from '../../helpers';

const INITIAL_STATE = {
  authentication: {
    userId: null,
    email: null,
    authToken: null,
    refreshToken: null
  },
  requestError: ''
};

const LOGIN_FORM_HEADER = 'Enter your credentials';

const LOGIN_FORM_NAME = 'LoginForm';

const LOGIN_FORM_CONFIG = {
  email: {
    element: 'input',
    config: {
      type: 'email',
      label: 'e-mail',
      placeholder: 'Your e-mail',
      validation: {
        required: true,
        isEmail: emailValidationPattern
      }
    }
  },
  password: {
    element: 'input',
    config: {
      type: 'password',
      label: 'password',
      placeholder: 'Your password',
      validation: {
        required: true,
        minLength: 7
      }
    }
  }
};

const LOGIN_FORM_SUBMIT_BUTTON = {
  labels: [ 'SIGNUP', 'LOGIN' ],
  css: [
    'Login'
  ],
  click: 'loginFormSubmitHandler'
};

const LOGIN_FORM_MODE_BUTTON = {
  label: 'SWITCH MODE',
  css: [
    'Mode'
  ],
  click: 'loginFormModeSwitchHandler'
};

const LOCAL_STORAGE_AUTHENTICATION_ITEM = 'authentication';

const REQUEST_USER_AUTHENTICATION = 'REQUEST_USER_AUTHENTICATION';
const REQUEST_USER_AUTHENTICATION_SUCCESS = 'REQUEST_USER_AUTHENTICATION_SUCCESS';
const REQUEST_USER_AUTHENTICATION_ERROR = 'REQUEST_USER_AUTHENTICATION_ERROR';
const DROP_USER_AUTHENTICATION = 'DROP_USER_AUTHENTICATION';
const CHECK_USER_AUTHENTICATION_STATUS = 'CHECK_USER_AUTHENTICATION_STATUS';

export {
  INITIAL_STATE,
  LOGIN_FORM_HEADER,
  LOGIN_FORM_NAME,
  LOGIN_FORM_CONFIG,
  LOGIN_FORM_SUBMIT_BUTTON,
  LOGIN_FORM_MODE_BUTTON,
  LOCAL_STORAGE_AUTHENTICATION_ITEM,
  REQUEST_USER_AUTHENTICATION,
  REQUEST_USER_AUTHENTICATION_SUCCESS,
  REQUEST_USER_AUTHENTICATION_ERROR,
  DROP_USER_AUTHENTICATION,
  CHECK_USER_AUTHENTICATION_STATUS
};
