// LIBS
import React, { Component }   from 'react';
import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';
import PropTypes              from 'prop-types';
// COMMON
import { routerPaths }        from '../../configs';
import { Button, Alert }      from '../../ui';
import { loginActions }       from '../actions';
import { loginSelectors }     from '../selectors';
import {
  isFormValidPropName,
  formConfigToFormState,
  formConfigToFormView,
  updateFormValidationState,
  updateFormFieldState,
  formStateToSubmitData
}                             from '../../helpers';
// COMPONENT
import {
  LOGIN_FORM_HEADER,
  LOGIN_FORM_NAME,
  LOGIN_FORM_CONFIG,
  LOGIN_FORM_SUBMIT_BUTTON,
  LOGIN_FORM_MODE_BUTTON
}                             from './product-login-constants';
import styles                 from './product-login.module.css';

class ProductLoginContainer extends Component {
  state = {
    ...formConfigToFormState(LOGIN_FORM_NAME, LOGIN_FORM_CONFIG),
    isSignUp: false
  };
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { isAuthenticated, history } = this.props;
    if(isAuthenticated) {
      history.replace(routerPaths.ROOT);
    }
  }
  
  fieldChangeHandler = (fieldName, fieldValue) => {
    this.setState((formState) => updateFormFieldState(
      LOGIN_FORM_NAME,
      LOGIN_FORM_CONFIG,
      formState,
      fieldName,
      fieldValue)
    );
    
    this.setState((formState) => updateFormValidationState(LOGIN_FORM_NAME, formState));
  };
  
  loginFormSubmitHandler = (event) => {
    event.preventDefault();
    
    const { history, onUserAuthenticationRequest } = this.props;
    const userCredentials = formStateToSubmitData(LOGIN_FORM_NAME, this.state);
    onUserAuthenticationRequest(userCredentials, this.state.isSignUp, history);
  };
  
  loginFormModeSwitchHandler = (event) => {
    event.preventDefault();
    this.setState(({ isSignUp }) => ({
      isSignUp: !isSignUp
    }));
  };
  
  render() {
    // TODO: clear errors on unmount, include into validation
      const { requestError } = this.props;
    const loginFormView = formConfigToFormView(
      LOGIN_FORM_NAME,
      LOGIN_FORM_CONFIG,
      this.state,
      this.fieldChangeHandler
    );
    
    const isLoginFormValid = this.state[isFormValidPropName(LOGIN_FORM_NAME)];
    
    const loginFormButtons = (
      <>
        <Button classNames={LOGIN_FORM_MODE_BUTTON.css}
                click={this[LOGIN_FORM_MODE_BUTTON.click]}>
          {LOGIN_FORM_MODE_BUTTON.label}
        </Button>
        <Button classNames={LOGIN_FORM_SUBMIT_BUTTON.css} disable={!isLoginFormValid}
                click={this[LOGIN_FORM_SUBMIT_BUTTON.click]}>
          {this.state.isSignUp ? LOGIN_FORM_SUBMIT_BUTTON.labels[0] : LOGIN_FORM_SUBMIT_BUTTON.labels[1]}
        </Button>
      </>
    );
    
    return (
      <div className={styles.Form}>
        {/* TODO: requestError handling on route change */}
        <h3 className={styles.FormHeader}>{LOGIN_FORM_HEADER}</h3>
        <form>
          {loginFormView}
          <div className={styles.FormButtons}>
            {loginFormButtons}
          </div>
        </form>
        {requestError
          ? <Alert>Error: {requestError.split('_').join(' ').toLowerCase()}</Alert>
          : null
        }
      </div>
    );
  };
}

// TODO: collect all proptypes in 1 place
ProductLoginContainer.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  requestError: PropTypes.string.isRequired,
  onUserAuthenticationRequest: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  isAuthenticated: loginSelectors.getAuthenticationToken(state) !== null,
  requestError: state.loginReducer.requestError
});

const mapDispatchToProps = (dispatch) => ({
  onUserAuthenticationRequest: bindActionCreators(loginActions.requestUserAuthentication, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductLoginContainer);
