import { updateState } from '../../helpers';
import {
  INITIAL_STATE,
  REQUEST_USER_AUTHENTICATION_SUCCESS,
  REQUEST_USER_AUTHENTICATION_ERROR,
  DROP_USER_AUTHENTICATION
}                      from './product-login-constants';

const setAuthenticationData = (state, authentication) =>
  updateState(state, { authentication });

const setAuthenticationError = (state, requestError) =>
  updateState(state, { requestError });

const productLoginReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case REQUEST_USER_AUTHENTICATION_SUCCESS:
      return setAuthenticationData(state, action.authentication);
    case REQUEST_USER_AUTHENTICATION_ERROR:
      return setAuthenticationError(state, action.requestError);
    case DROP_USER_AUTHENTICATION:
      return INITIAL_STATE;
    default:
      return state;
  }
};

export default productLoginReducer;
