import productMainReducer    from './product-main/product-main-reducer';
import productBuilderReducer from './product-builder/product-builder-reducer';
import productOrdersReducer  from './product-orders/product-orders-reducer';
import productLoginReducer   from './product-login/product-login-reducer';

export {
  productMainReducer as mainReducer,
  productBuilderReducer as builderReducer,
  productOrdersReducer as ordersReducer,
  productLoginReducer as loginReducer
}
