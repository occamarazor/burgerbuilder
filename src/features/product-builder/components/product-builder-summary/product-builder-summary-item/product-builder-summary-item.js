import React          from 'react';
import PropTypes      from 'prop-types';
import { capitalize } from '../../../../../helpers';
import styles         from './product-builder-summary-item.module.css';

const ProductBuilderSummaryItem = ({ type, amount }) => (
  <div className={styles.SummaryItem}>
    <h4>{capitalize(type)}:</h4>
    <p>{amount}</p>
  </div>
);

// TODO: collect all proptypes in 1 place
ProductBuilderSummaryItem.propTypes = {
  type:   PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired
};

export default ProductBuilderSummaryItem;