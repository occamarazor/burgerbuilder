import React, { Fragment }           from 'react';
import PropTypes                     from 'prop-types';
import { ProductPrice }              from '../../../../common';
import { Button }                    from '../../../../ui';
import { ProductBuilderSummaryItem } from '../';
import {
  HEADER_LABEL,
  DESC_LABEL,
  CONFIRM_LABEL,
  BUTTONS
}                                    from './product-builder-summary-constants';
import styles                        from './product-builder-summary.module.css';

const ProductBuilderSummary = ({ productSummary, productPrice, ...props }) => {
  const productSummaryItems = productSummary.map((ps) => (
    <ProductBuilderSummaryItem key={ps.type} type={ps.type} amount={ps.amount}/>
  ));
  
  const productSummaryButtons = BUTTONS.map((sb) => (
    <Button key={sb.label}
            classNames={sb.css}
            click={props[sb.click]}>
      {sb.label}
    </Button>));
  
  return (
    <Fragment>
      <h2 className={styles.SummaryHeader}>{HEADER_LABEL}</h2>
      <p className={styles.SummaryDescription}>{DESC_LABEL}</p>
      
      <div className={styles.SummaryItems}>
        {productSummaryItems}
      </div>
      
      <ProductPrice productPrice={productPrice}/>
      
      <p className={styles.SummaryConfirmation}>{CONFIRM_LABEL}</p>
      <div className={styles.SummaryButtons}>
        {productSummaryButtons}
      </div>
    </Fragment>
  )
};

// TODO: collect all proptypes in 1 place
ProductBuilderSummary.propTypes = {
  productSummary:        PropTypes.arrayOf(
    PropTypes.exact({
      type:   PropTypes.string.isRequired,
      amount: PropTypes.number.isRequired
    }).isRequired
  ).isRequired,
  productPrice:          PropTypes.number.isRequired,
  onPurchaseCancelBtn:   PropTypes.func.isRequired,
  onPurchaseContinueBtn: PropTypes.func.isRequired
};

export default ProductBuilderSummary;