const HEADER_LABEL = 'Your order';

const DESC_LABEL = 'A delicious burger with the following ingredients:';

const CONFIRM_LABEL = 'Continue checkout?';

const BUTTONS = [
  {
    label: 'CANCEL',
    css:   [ 'Cancel' ],
    click: 'onPurchaseCancelBtn'
  },
  {
    label: 'CONTINUE',
    css:   [ 'Continue' ],
    click: 'onPurchaseContinueBtn'
  }
];

export {
  HEADER_LABEL,
  DESC_LABEL,
  CONFIRM_LABEL,
  BUTTONS
}