import React, { Component }           from 'react';
import PropTypes                      from 'prop-types';
import { capitalize }                 from '../../../../helpers';
import { Button }                     from '../../../../ui';
import { ProductBuilderControlsItem } from '../';
import { BUTTONS }                    from './product-builder-controls-constants';
import styles                         from './product-builder-controls.module.css';

class ProductBuilderControls extends Component {
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    const { productControls, isProductPurchasable } = this.props;
    return JSON.stringify(productControls) !== JSON.stringify(nextProps.productControls)
      || isProductPurchasable !== nextProps.isProductPurchasable;
  };
  
  render() {
    const {
      productControls,
      isProductPurchasable,
      onAddIngredient,
      onRemoveIngredient
    } = this.props;
    
    const productControlsItems = productControls.map((pc) => (
      <ProductBuilderControlsItem key={pc.type}
                                  label={capitalize(pc.type)}
                                  isDisabled={pc.isDisabled}
                                  onAddIngredientClick={onAddIngredient.bind(this, pc)}
                                  onRemoveIngredientClick={onRemoveIngredient.bind(this, pc)}/>
    ));
    
    const productControlsButtons = BUTTONS.map((cb) => (
      <Button key={cb.label}
              classNames={cb.css}
              disable={!isProductPurchasable}
              click={this.props[cb.click]}>
        {cb.label}
      </Button>
    ));
    
    return (
      <div className={styles.BuildControls}>
        {productControlsItems}
        <div className={styles.ControlsButtons}>
          {productControlsButtons}
        </div>
      </div>
    );
  };
}

// TODO: collect all proptypes in 1 place
ProductBuilderControls.propTypes = {
  productControls: PropTypes.arrayOf(
    PropTypes.exact({
      type: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      isDisabled: PropTypes.bool.isRequired
    }).isRequired
  ).isRequired,
  isProductPurchasable: PropTypes.bool.isRequired,
  onOrderBtnClick: PropTypes.func.isRequired,
  onResetBtnClick: PropTypes.func.isRequired,
  onAddIngredient: PropTypes.func.isRequired,
  onRemoveIngredient: PropTypes.func.isRequired
};

export default ProductBuilderControls;