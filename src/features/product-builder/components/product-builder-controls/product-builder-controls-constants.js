const BUTTONS = [
  {
    label: 'RESET',
    css:   [
      'Controls',
      'ResetProduct'
    ],
    click: 'onResetBtnClick'
  },
  {
    label: 'ORDER',
    css:   [
      'Controls',
      'OrderProduct'
    ],
    click: 'onOrderBtnClick'
  }
];

export {
  BUTTONS
}