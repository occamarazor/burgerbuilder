const BUTTONS = [
  {
    label:   'Less',
    css:     [
      'Control',
      'RemoveIngredient'
    ],
    disable: 'isDisabled',
    click:   'onRemoveIngredientClick'
  },
  {
    label: 'More',
    css:   [
      'Control',
      'AddIngredient'
    ],
    click: 'onAddIngredientClick'
  }
];

export {
  BUTTONS
}