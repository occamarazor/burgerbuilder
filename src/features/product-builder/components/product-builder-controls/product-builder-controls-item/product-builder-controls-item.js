import React, { Component } from 'react';
import PropTypes            from 'prop-types';
import { Button }           from '../../../../../ui';
import { BUTTONS }          from './product-builder-controls-item-constants';
import styles               from './product-builder-controls-item.module.css';

class ProductBuilderControlsItem extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    const { label, isDisabled } = this.props;
    return label !== nextProps.label || isDisabled !== nextProps.isDisabled;
  }
  
  render() {
    const productControlsItemButtons = BUTTONS.map((cib) => (
      <Button key={cib.label}
              classNames={cib.css}
              disable={this.props[cib.disable]}
              click={this.props[cib.click]}>
        {cib.label}
      </Button>
    ));
    
    return (
      <div className={styles.BuildControl}>
        <div className={styles.Label}>{this.props.label}</div>
        {productControlsItemButtons}
      </div>
    );
  }
}

// TODO: collect all proptypes in 1 place
ProductBuilderControlsItem.propTypes = {
  label:                   PropTypes.string.isRequired,
  isDisabled:              PropTypes.bool.isRequired,
  onAddIngredientClick:    PropTypes.func.isRequired,
  onRemoveIngredientClick: PropTypes.func.isRequired
};

export default ProductBuilderControlsItem;