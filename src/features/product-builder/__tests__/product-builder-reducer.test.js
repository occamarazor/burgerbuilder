import productBuilderReducer, { INITIAL_STATE } from '../product-builder-reducer';
import {
  GET_AVAILABLE_INGREDIENTS,
  ADD_PRODUCT_INGREDIENT,
  REMOVE_PRODUCT_INGREDIENT,
  RESET_PRODUCT_INGREDIENTS,
} from '../product-builder-constants';

describe('productBuilderReducer', () => {
  it('should return initial state', () => {
    const state = undefined;
    const action = { type: 'ANY_ACTION' };

    expect(productBuilderReducer(state, action)).toEqual(INITIAL_STATE);
  });

  it('should store availableIngredients', () => {
    const availableIngredients = [
      { price: 1.3, type: 'bacon' },
      { price: 0.5, type: 'cheese' },
    ];
    const action = { type: GET_AVAILABLE_INGREDIENTS, payload: { availableIngredients } };

    expect(productBuilderReducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      availableIngredients,
    });
  });

  it('should add productIngredient & adjust productPrice', () => {
    const { productIngredients, productPrice } = INITIAL_STATE;
    const newIngredient = { price: 0.9, type: 'meat' };
    const action = { type: ADD_PRODUCT_INGREDIENT, payload: { newIngredient } };

    expect(productBuilderReducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      productIngredients: [...productIngredients, newIngredient],
      productPrice: +(productPrice + newIngredient.price).toFixed(2),
    });
  });

  it('should remove productIngredient & adjust productPrice', () => {
    const state = {
      ...INITIAL_STATE,
      productIngredients: [
        { price: 0.9, type: 'meat' },
        { price: 0.4, type: 'tomato' },
      ],
      productPrice: 1.3,
    };
    const { productIngredients, productPrice } = state;
    const ingredientId = productIngredients.length - 1; // last ingredient ID
    const ingredientPrice = productIngredients[ingredientId].price;
    const action = { type: REMOVE_PRODUCT_INGREDIENT, payload: { ingredientId } };

    expect(productBuilderReducer(state, action)).toEqual({
      ...state,
      productIngredients: productIngredients.filter((pi, i) => i !== ingredientId),
      productPrice: +(productPrice - ingredientPrice).toFixed(2),
    });
  });

  it('should clear productIngredients & reset productPrice', () => {
    const state = {
      ...INITIAL_STATE,
      productIngredients: [
        { price: 0.5, type: 'egg' },
        { price: 0.2, type: 'potato' },
      ],
      productPrice: 0.7,
    };
    const action = { type: RESET_PRODUCT_INGREDIENTS };

    expect(productBuilderReducer(state, action)).toEqual(INITIAL_STATE);
  });
});
