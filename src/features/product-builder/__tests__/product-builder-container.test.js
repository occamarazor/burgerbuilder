import React from 'react';
import { shallow } from 'enzyme';

import ProductBuilderContainer from '../product-builder-container';
import ProductBuilderControls from '../components/product-builder-controls/product-builder-controls';

describe('<ProductBuilderContainer />', () => {
  let props;
  let wrapper;

  beforeEach(() => {
    props = {
      availableIngredients: [],
      isAuthenticated: false,
      isDataLoading: false,
      isRequestError: false,
      onAvailableIngredientsGet: () => {},
      onProductIngredientAdd: () => {},
      onProductIngredientRemove: () => {},
      onProductIngredientsReset: () => {},
      onRequestErrorUpdate: () => {},
      productIngredients: [],
      productPrice: 0,
    };
    wrapper = shallow(<ProductBuilderContainer {...props} />);
  });

  it('should render without crashing', () => {
    shallow(<ProductBuilderContainer {...props} />);
  });

  it('should render <ProductBuilderControls/ > when availableIngredients received', () => {
    const availableIngredients = [{ price: 1.3, type: 'bacon' }];

    wrapper.setProps({ availableIngredients });
    expect(wrapper.find(ProductBuilderControls)).toHaveLength(1);
  });
});
