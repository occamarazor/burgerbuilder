// LIBS
import { connect } from 'react-redux';
// COMMON
import { mainActions, builderActions } from '../actions';
import { loginSelectors } from '../selectors';
// COMPONENTS
import ProductBuilderContainer from './product-builder-container';

// TODO: use selectors
const mapStateToProps = (state) => ({
  isDataLoading: state.mainReducer.isDataLoading,
  isRequestError: state.mainReducer.isRequestError,
  availableIngredients: state.builderReducer.availableIngredients,
  productIngredients: state.builderReducer.productIngredients,
  productPrice: state.builderReducer.productPrice,
  isAuthenticated: loginSelectors.getAuthenticationToken(state) !== null,
});

// TODO: use bindActionCreators
const mapDispatchToProps = (dispatch) => ({
  // onRequestErrorUpdate: bindActionCreators(mainActions.updateRequestError, dispatch),
  onRequestErrorUpdate: (payload) => dispatch(mainActions.updateRequestError(payload)),
  onAvailableIngredientsGet: () => dispatch(builderActions.getAvailableIngredients()),
  onProductIngredientAdd: (payload) => dispatch(builderActions.addProductIngredient(payload)),
  onProductIngredientRemove: (payload) => dispatch(builderActions.removeProductIngredient(payload)),
  onProductIngredientsReset: () => dispatch(builderActions.resetProductIngredients()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductBuilderContainer);
