import {
  GET_AVAILABLE_INGREDIENTS,
  ADD_PRODUCT_INGREDIENT,
  REMOVE_PRODUCT_INGREDIENT,
  RESET_PRODUCT_INGREDIENTS,
} from './product-builder-constants';
import { updateState } from '../../helpers';

export const INITIAL_STATE = {
  // Dummy data for a nice look while data is loading
  availableIngredients: [
    { price: 0.5, type: 'egg' },
    { price: 0.2, type: 'potato' },
    { price: 0.9, type: 'meat' },
    { price: 0.4, type: 'tomato' },
  ],
  productIngredients: [],
  productPrice: 0,
};

const getIngredients = (prevState, action) =>
  updateState(prevState, {
    availableIngredients: action.payload.availableIngredients,
  });

const addIngredient = (prevState, action) => {
  const { productIngredients, productPrice } = prevState;
  const { newIngredient } = action.payload;

  return updateState(prevState, {
    // TODO: helper functions
    productIngredients: [...productIngredients, newIngredient],
    productPrice: +(productPrice + newIngredient.price).toFixed(2),
  });
};

const removeIngredient = (prevState, action) => {
  const { productIngredients, productPrice } = prevState;
  const { ingredientId } = action.payload;
  const ingredientPrice = productIngredients[ingredientId].price;

  return updateState(prevState, {
    // TODO: helper functions
    productIngredients: productIngredients.filter((pi, i) => i !== ingredientId),
    productPrice: +(productPrice - ingredientPrice).toFixed(2),
  });
};

const resetIngredients = (prevState) =>
  updateState(INITIAL_STATE, {
    availableIngredients: prevState.availableIngredients,
  });

const productBuilderReducer = (prevState = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_AVAILABLE_INGREDIENTS:
      return getIngredients(prevState, action);
    case ADD_PRODUCT_INGREDIENT:
      return addIngredient(prevState, action);
    case REMOVE_PRODUCT_INGREDIENT:
      return removeIngredient(prevState, action);
    case RESET_PRODUCT_INGREDIENTS:
      return resetIngredients(prevState);
    default:
      return prevState;
  }
};

export default productBuilderReducer;
