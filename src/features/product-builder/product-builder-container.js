// LIBS
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
// COMMON
import { routerPaths } from '../../configs';
import { ProductIngredients, ProductPrice } from '../../common';
import { Alert, Modal } from '../../ui';
// COMPONENT
import { ProductBuilderControls, ProductBuilderSummary } from './components';
import { INITIAL_STATE, REQUEST_ERROR_LABEL, DATA_EMPTY_LABEL } from './product-builder-constants';
import styles from './product-builder.module.css';

class ProductBuilderContainer extends PureComponent {
  state = INITIAL_STATE;

  static getDerivedStateFromProps(nextProps, prevState) {
    // now isProductPurchasable used in 1 place, getDerivedStateFromProps left for demo purpose!
    const isProductPurchasable = nextProps.productIngredients.length > 0;
    return prevState.isProductPurchasable !== isProductPurchasable
      ? { isProductPurchasable }
      : null;
  }

  componentDidMount() {
    this.props.onAvailableIngredientsGet();
  }

  // TODO: remove, each container must have it's own local instance
  componentWillUnmount() {
    const { isRequestError, onRequestErrorUpdate } = this.props;
    if (isRequestError) {
      onRequestErrorUpdate({ isRequestError: false });
    }
  }

  purchaseModeToggleHandler = () => {
    const { isAuthenticated, history } = this.props;
    isAuthenticated
      ? this.setState((state) => ({
          isPurchaseModeActive: !state.isPurchaseModeActive,
        }))
      : history.push(routerPaths.LOGIN);
  };

  purchaseContinueHandler = () => {
    this.props.history.push(routerPaths.CHECKOUT);
  };

  addIngredientHandler = (productControl) => {
    const payload = {
      newIngredient: {
        type: productControl.type,
        price: productControl.price,
      },
    };

    this.props.onProductIngredientAdd(payload);
  };

  removeIngredientHandler = (productControl) => {
    const payload = {
      ingredientId: this.props.productIngredients
        .map((pi) => pi.type)
        .lastIndexOf(productControl.type),
    };

    this.props.onProductIngredientRemove(payload);
  };

  resetProductIngredientsHandler = () => {
    this.props.onProductIngredientsReset();
  };

  render() {
    const {
      isDataLoading,
      isRequestError,
      availableIngredients,
      productIngredients,
      productPrice,
    } = this.props;
    const { isProductPurchasable, isPurchaseModeActive } = this.state;
    const controls = availableIngredients.map((ai) => ({
      ...ai,
      isDisabled: productIngredients.filter((pi) => ai.type === pi.type).length <= 0,
    }));
    const summary = availableIngredients.map((ai) => ({
      type: ai.type,
      amount: productIngredients.filter((pi) => ai.type === pi.type).length,
    }));

    let builderPage = (
      <>
        <ProductIngredients productIngredients={productIngredients} />
        <div className={styles.BuilderControls}>
          <ProductPrice productPrice={productPrice} />
          <ProductBuilderControls
            productControls={controls}
            isProductPurchasable={isProductPurchasable}
            onOrderBtnClick={this.purchaseModeToggleHandler}
            onResetBtnClick={this.resetProductIngredientsHandler}
            onAddIngredient={this.addIngredientHandler}
            onRemoveIngredient={this.removeIngredientHandler}
          />
        </div>
        <Modal isVisible={isPurchaseModeActive} onClose={this.purchaseModeToggleHandler}>
          <ProductBuilderSummary
            productSummary={summary}
            productPrice={productPrice}
            onPurchaseCancelBtn={this.purchaseModeToggleHandler}
            onPurchaseContinueBtn={this.purchaseContinueHandler}
          />
        </Modal>
      </>
    );

    if (isRequestError) {
      builderPage = <Alert>{REQUEST_ERROR_LABEL}</Alert>;
    } else if (!isDataLoading && availableIngredients.length <= 0) {
      builderPage = <Alert>{DATA_EMPTY_LABEL}</Alert>;
    }

    return builderPage;
  }
}

// TODO: collect all proptypes in 1 place
ProductBuilderContainer.propTypes = {
  isDataLoading: PropTypes.bool.isRequired,
  isRequestError: PropTypes.bool.isRequired,
  availableIngredients: PropTypes.arrayOf(
    PropTypes.exact({
      type: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
    }).isRequired
  ).isRequired,
  productIngredients: PropTypes.arrayOf(
    PropTypes.exact({
      type: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
    }).isRequired
  ).isRequired,
  productPrice: PropTypes.number.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  onRequestErrorUpdate: PropTypes.func.isRequired,
  onAvailableIngredientsGet: PropTypes.func.isRequired,
  onProductIngredientAdd: PropTypes.func.isRequired,
  onProductIngredientRemove: PropTypes.func.isRequired,
  onProductIngredientsReset: PropTypes.func.isRequired,
};

export default ProductBuilderContainer;
