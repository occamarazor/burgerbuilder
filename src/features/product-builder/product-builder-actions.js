import { http }        from '../../configs';
import { mainActions } from '../actions';
import {
  GET_AVAILABLE_INGREDIENTS,
  ADD_PRODUCT_INGREDIENT,
  REMOVE_PRODUCT_INGREDIENT,
  RESET_PRODUCT_INGREDIENTS
}                      from './product-builder-constants';

const updateIngredients = (payload) => ({
  type: GET_AVAILABLE_INGREDIENTS,
  payload
});

const getAvailableIngredients = () => (dispatch) => {
  dispatch(mainActions.updateDataLoader({ isDataLoading: true }));
  
  // TODO: promise to async await
  // TODO: slow network imitation
  setTimeout(() => {
    http
      .app
      .get('/availableIngredients.json')
      .then(({ data }) => {
        dispatch(updateIngredients({ availableIngredients: data ? data : [] }));
        dispatch(mainActions.updateDataLoader({ isDataLoading: false }));
      })
      .catch((error) => {
        console.log(error);
        dispatch(mainActions.updateDataLoader({ isDataLoading: false }));
        dispatch(mainActions.updateRequestError({ isRequestError: true }));
      });
  }, 1000);
};

const addProductIngredient = (payload) => ({
  type: ADD_PRODUCT_INGREDIENT,
  payload
});

const removeProductIngredient = (payload) => ({
  type: REMOVE_PRODUCT_INGREDIENT,
  payload
});

const resetProductIngredients = () => ({
  type: RESET_PRODUCT_INGREDIENTS
});

export {
  getAvailableIngredients,
  addProductIngredient,
  removeProductIngredient,
  resetProductIngredients
};