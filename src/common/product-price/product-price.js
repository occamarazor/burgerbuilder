import React, { PureComponent } from 'react';
import PropTypes                from 'prop-types';
import { PRICE_LABEL }          from './product-price-constants';
import styles                   from './product-price.module.css';

class ProductPrice extends PureComponent {
  render() {
    return (
      <h3 className={styles.Price}>
        {PRICE_LABEL}{this.props.productPrice.toFixed(2)}
      </h3>
    );
  };
}

// TODO: collect all proptypes in 1 place
ProductPrice.propTypes = {
  productPrice: PropTypes.number.isRequired
};

export default ProductPrice;