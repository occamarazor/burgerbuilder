import React, { Component } from 'react';
import { Route }            from 'react-router-dom';
import PropTypes            from 'prop-types';

class RouteWithSubRoutes extends Component {
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return false;
  }
  
  render() {
    const { path, exact, routes } = this.props;
    
    return (
      <Route
        path={path}
        exact={exact}
        render={(componentProps) => (
          <this.props.component {...componentProps} routes={routes}/>
        )}
      />
    );
  };
}

// TODO: collect all proptypes in 1 place
RouteWithSubRoutes.propTypes = {
  path: PropTypes.string.isRequired,
  exact: PropTypes.bool,
  component: PropTypes.elementType.isRequired,
  routes: PropTypes.arrayOf(
    PropTypes.exact({
      path: PropTypes.string.isRequired,
      exact: PropTypes.bool,
      component: PropTypes.elementType.isRequired
    }).isRequired
  )
};

export default RouteWithSubRoutes;
