import RouteWithSubRoutes from './route-with-subroutes/route-with-sub-routes';
import ProductIngredients from './product-ingredients/product-ingredients';
import ProductPrice       from './product-price/product-price';

export {
  RouteWithSubRoutes,
  ProductIngredients,
  ProductPrice
};
