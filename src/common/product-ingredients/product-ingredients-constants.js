const INGREDIENTS_TYPES = {
  breadTop:    'breadTop',
  breadBottom: 'breadBottom',
  meat:        'meat',
  cheese:      'cheese',
  salad:       'salad',
  bacon:       'bacon'
};

const INGREDIENTS_NONE = 'Please add ingredients!';

export {
  INGREDIENTS_TYPES,
  INGREDIENTS_NONE
}