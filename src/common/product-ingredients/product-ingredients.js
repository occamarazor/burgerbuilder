import React, { PureComponent }                from 'react';
import PropTypes                               from 'prop-types';
import { INGREDIENTS_TYPES, INGREDIENTS_NONE } from './product-ingredients-constants';
import { ProductIngredientsItem }              from './components';
import styles                                  from './product-ingredients.module.css';

class ProductIngredients extends PureComponent {
  render() {
    const { productIngredients } = this.props;
    const productIngredientsItems = productIngredients.map((pi, i) => (
      <ProductIngredientsItem key={i} type={pi.type}/>
    ));
    
    return (
      <div className={styles.Burger}>
        <ProductIngredientsItem type={INGREDIENTS_TYPES.breadTop}/>
        {
          productIngredientsItems.length === 0
            ? <p>{INGREDIENTS_NONE}</p>
            : productIngredientsItems
        }
        <ProductIngredientsItem type={INGREDIENTS_TYPES.breadBottom}/>
      </div>
    );
  }
}

// TODO: collect all proptypes in 1 place
ProductIngredients.propTypes = {
  productIngredients: PropTypes.arrayOf(
    PropTypes.exact({
      type:  PropTypes.string.isRequired,
      price: PropTypes.number.isRequired
    }).isRequired
  ).isRequired
};

export default ProductIngredients;