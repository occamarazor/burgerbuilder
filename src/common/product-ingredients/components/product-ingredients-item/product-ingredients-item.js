import React, { PureComponent } from 'react';
import PropTypes                from 'prop-types';
import styles                   from './product-ingredients-item.module.css';
import { INGREDIENTS_TYPES } from '../../product-ingredients-constants';
import { capitalize }        from '../../../../helpers';

class ProductIngredientsItem extends PureComponent {
  render() {
    const { type } = this.props;
    let ingredient = (
      <div className={styles.Default}>
        {type}
      </div>
    );
    
    if (type === INGREDIENTS_TYPES.breadTop) {
      ingredient = (
        <div className={styles[capitalize(type)]}>
          <div className={styles.Seeds1}/>
          <div className={styles.Seeds2}/>
        </div>
      );
    } else if (type in INGREDIENTS_TYPES) {
      ingredient = <div className={styles[capitalize(type)]}/>
    }
    
    return ingredient;
  }
}

// TODO: collect all proptypes in 1 place
ProductIngredientsItem.propTypes = {
  type: PropTypes.string.isRequired
};

export default ProductIngredientsItem;