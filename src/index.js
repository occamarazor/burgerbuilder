import React                                                        from 'react';
import ReactDOM                                                     from 'react-dom';
import { BrowserRouter }                                            from 'react-router-dom';
import { createStore, combineReducers, compose, applyMiddleware }   from 'redux';
// import logger                                                       from 'redux-logger';
import thunk                                                        from 'redux-thunk';
import { Provider }                                                 from 'react-redux';
import WebFont                                                      from 'webfontloader';
import * as serviceWorker                                           from './serviceWorker';
import { mainReducer, builderReducer, ordersReducer, loginReducer } from './features/reducers';
import App                                                          from './app';
import './index.css';

WebFont.load({
  google: {
    families: [
      'Open Sans:300,700',
      'sans-serif'
    ]
  }
});

const rootReducer = combineReducers({
  mainReducer,
  builderReducer,
  ordersReducer,
  loginReducer
});

const composeEnhancers = process.env.NODE_ENV === 'development'
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  : compose;

const store = createStore(rootReducer, composeEnhancers(
  // applyMiddleware(logger, thunk)
  applyMiddleware(thunk)
));

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
